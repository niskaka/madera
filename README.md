# MADERA
# == Création du projet Madera == #

I) Télécharger Symfony sur "https://symfony.com/" ( le .exe pour windows )
II) Téléchargez composer sur "https://getcomposer.org/", cliquez sur "download"

III) Créez un dossier "madera" où vous le souhaitez, rentrez dedans et ouvrez un invité de commandes git-bash



IV) tapez les commandes : "git clone https://gitlab.com/niskaka/madera",
                          "git pull",
                          "git checkout dev",


V) paré à coder !! 

VI) Géneration de la base de données
    dans votre php.ini 'votrepath PHP'/php.ini ajouter l'extention "pdo_sqlite" (ctrl+f pour trouver la ligne)
    puis dans le repertoir 'skeleton' :  - php bin/console make:migration
                                         - php bin/console doctrine:migrations:migrate
                                         - php bin/console doctrine:fixtures:load