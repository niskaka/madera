// Vérification de la cohérence des deux champs password à l'inscription
$("#new_edit_utilisateur").on('submit', function () {
    if($("#utilisateur_password").val() != $("#verifpass").val()) {
        //implémntez votre code
        alert("Les deux mots de passe saisies sont différents");
        alert("Merci de renouveler l'opération");
        return false;
    }
})