<?php

namespace App\Entity;

use App\Repository\ComposantModuleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComposantModuleRepository::class)
 */
class ComposantModule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Composant::class, inversedBy="composantModules", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $composant;

    /**
     * @ORM\ManyToOne(targetEntity=Module::class, inversedBy="composantModules", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $module;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComposant(): ?Composant
    {
        return $this->composant;
    }

    public function setComposant(?Composant $composant): self
    {
        $this->composant = $composant;

        return $this;
    }

    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }
}
