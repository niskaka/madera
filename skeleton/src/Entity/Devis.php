<?php

namespace App\Entity;

use App\Repository\DevisRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DevisRepository::class)
 */
class Devis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ref;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_creation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_modification;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $margeCommerciale;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $margeEntreprise;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prixHT;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prixTTC;

    /**
     * @ORM\ManyToOne(targetEntity=Etat::class, inversedBy="devis", cascade={"persist"})
     */
    private $etat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdDevis(): ?int
    {
        return $this->idDevis;
    }

    public function setIdDevis(int $idDevis): self
    {
        $this->idDevis = $idDevis;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->date_creation;
    }

    public function setDateCreation(\DateTimeInterface $date_creation): self
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->date_modification;
    }

    public function setDateModification(\DateTimeInterface $date_modification): self
    {
        $this->date_modification = $date_modification;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getMargeCommerciale(): ?string
    {
        return $this->margeCommerciale;
    }

    public function setMargeCommerciale(string $margeCommerciale): self
    {
        $this->margeCommerciale = $margeCommerciale;

        return $this;
    }

    public function getMargeEntreprise(): ?string
    {
        return $this->margeEntreprise;
    }

    public function setMargeEntreprise(?string $margeEntreprise): self
    {
        $this->margeEntreprise = $margeEntreprise;

        return $this;
    }

    public function getPrixHT(): ?string
    {
        return $this->prixHT;
    }

    public function setPrixHT(string $prixHT): self
    {
        $this->prixHT = $prixHT;

        return $this;
    }

    public function getPrixTTC(): ?string
    {
        return $this->prixTTC;
    }

    public function setPrixTTC(string $prixTTC): self
    {
        $this->prixTTC = $prixTTC;

        return $this;
    }

    public function getEtat(): ?Etat
    {
        return $this->etat;
    }

    public function setEtat(?Etat $etat): self
    {
        $this->etat = $etat;

        return $this;
    }
}
