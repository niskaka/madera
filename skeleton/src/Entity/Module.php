<?php

namespace App\Entity;

use App\Repository\ModuleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ModuleRepository::class)
 */
class Module
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_creation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_modification;

    /**
     * @ORM\OneToOne(targetEntity=Gamme::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $gamme;

    /**
     * @ORM\OneToMany(targetEntity=ComposantModule::class, mappedBy="module")
     */
    private $composantModules;

    public function __construct()
    {
        $this->composantModules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdModule(): ?int
    {
        return $this->idModule;
    }

    public function setIdModule(int $idModule): self
    {
        $this->idModule = $idModule;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->date_creation;
    }

    public function setDateCreation(\DateTimeInterface $date_creation): self
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->date_modification;
    }

    public function setDateModification(\DateTimeInterface $date_modification): self
    {
        $this->date_modification = $date_modification;

        return $this;
    }

    public function getGamme(): ?Gamme
    {
        return $this->gamme;
    }

    public function setGamme(Gamme $gamme): self
    {
        $this->gamme = $gamme;

        return $this;
    }

    /**
     * @return Collection|ComposantModule[]
     */
    public function getComposantModules(): Collection
    {
        return $this->composantModules;
    }

    public function addComposantModule(ComposantModule $composantModule): self
    {
        if (!$this->composantModules->contains($composantModule)) {
            $this->composantModules[] = $composantModule;
            $composantModule->setModule($this);
        }

        return $this;
    }

    public function removeComposantModule(ComposantModule $composantModule): self
    {
        if ($this->composantModules->removeElement($composantModule)) {
            // set the owning side to null (unless already changed)
            if ($composantModule->getModule() === $this) {
                $composantModule->setModule(null);
            }
        }

        return $this;
    }
}
