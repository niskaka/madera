<?php

namespace App\Entity;

use App\Repository\ComposantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComposantRepository::class)
 */
class Composant
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nature;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $caract;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $unit;

    /**
     * @ORM\ManyToMany(targetEntity=Fournisseur::class, inversedBy="composants")
     */
    private $fournisseurs;

    /**
     * @ORM\OneToMany(targetEntity=ComposantModule::class, mappedBy="composant")
     */
    private $composantModules;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="composants", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    public function __construct()
    {
        $this->fournisseurs = new ArrayCollection();
        $this->composantModules = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdcomposant(): ?int
    {
        return $this->idcomposant;
    }

    public function setIdcomposant(int $idcomposant): self
    {
        $this->idcomposant = $idcomposant;

        return $this;
    }

    public function getNature(): ?string
    {
        return $this->nature;
    }

    public function setNature(string $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    public function getCaract(): ?string
    {
        return $this->caract;
    }

    public function setCaract(string $caract): self
    {
        $this->caract = $caract;

        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @return Collection|Fournisseur[]
     */
    public function getFournisseurs(): Collection
    {
        return $this->fournisseurs;
    }

    public function addFournisseur(Fournisseur $fournisseur): self
    {
        if (!$this->fournisseurs->contains($fournisseur)) {
            $this->fournisseurs[] = $fournisseur;
        }

        return $this;
    }

    public function removeFournisseur(Fournisseur $fournisseur): self
    {
        $this->fournisseurs->removeElement($fournisseur);

        return $this;
    }

    /**
     * @return Collection|ComposantModule[]
     */
    public function getComposantModules(): Collection
    {
        return $this->composantModules;
    }

    public function addComposantModule(ComposantModule $composantModule): self
    {
        if (!$this->composantModules->contains($composantModule)) {
            $this->composantModules[] = $composantModule;
            $composantModule->setComposant($this);
        }

        return $this;
    }

    public function removeComposantModule(ComposantModule $composantModule): self
    {
        if ($this->composantModules->removeElement($composantModule)) {
            // set the owning side to null (unless already changed)
            if ($composantModule->getComposant() === $this) {
                $composantModule->setComposant(null);
            }
        }

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }
}
