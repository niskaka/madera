<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Commercial;
use App\Entity\Composant;
use App\Entity\Devis;
use App\Entity\Etat;
use App\Entity\Fournisseur;
use App\Entity\Gamme;
use App\Entity\Module;
use App\Entity\Projet;
use App\Entity\Statut;
use App\Entity\Type;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $OuvertE = new Etat();
        $OuvertE->setDescription("Ouvert");
        $manager->persist($OuvertE);
        $courE = new Etat();
        $courE->setDescription("En cours");
        $manager->persist($courE);
        $TermineE = new Etat();
        $TermineE->setDescription("Terminé");
        $manager->persist($TermineE);

        $OuvertS = new Statut();
        $OuvertS->setDescription("Ouvert");
        $manager->persist($OuvertS);
        $courS = new Statut();
        $courS->setDescription("En cours");
        $manager->persist($courS);
        $TermineS = new Statut();
        $TermineS->setDescription("Terminé");
        $manager->persist($TermineS);

        $Professionels = new Gamme();
        $Professionels->setName("Professionels");
        $manager->persist($Professionels);
        $Amateurs = new Gamme();
        $Amateurs->setName("Amateurs");
        $manager->persist($Amateurs);
        $confirme = new Gamme();
        $confirme->setName("Confirmés");
        $manager->persist($confirme);

        for ($i = 1; $i < 4; $i++) {
            $Type = new Type();
            $Type->setCaract("Caract" . $i);
            $Type->setUnite("unite" . $i);
            $manager->persist($Type);

            $Composant = new Composant();
            $Composant->setNature("Nature" . $i);
            $Composant->setCaract("Caract" . $i);
            $Composant->setUnit("unit" . $i);
            $Composant->setType($Type);
            $manager->persist($Composant);
        }
        for ($i = 1; $i < 4; $i++) {
           
        }
        $Client = new Client();
        $Client->setNom("DUPON");
        $Client->setPrenom("Jean");
        $Client->setAdresse($i . " Rue des Champs");
        $Client->setCodePostal('4500' . $i);
        $Client->setVille('Orléans');
        $Client->setEmail('client@email.com');
        $Client->setTelephone('060000000' . $i);
        $manager->persist($Client);

        $Commercial = new Commercial();
        $Commercial->setRef("REF-C" . $i);
        $Commercial->setNom("Nom" . $i);
        $Commercial->setPrenom("Prenom" . $i);
        $Commercial->setMp($i);
        $Commercial->setMail('commercial' . $i . '@email.com');
        $manager->persist($Commercial);

        $Fournisseur = new Fournisseur();
        $Fournisseur->setNom("Nom" . $i);
        $Fournisseur->setPrenom("Prenom" . $i);
        $Fournisseur->setAdresse($i . "Rue des Champs");
        $Fournisseur->setCodePostal('4500' . $i);
        $Fournisseur->setVille('Orléans');
        $Fournisseur->setEmail('fournisseur' . $i . '@email.com');
        $Fournisseur->setTelephone('060000000' . $i);
        $manager->persist($Fournisseur);

        for ($i = 1; $i < 4; $i++) {
            $product = new Devis();
            $product->setRef("REF-D" . $i);
            $product->setDateCreation(new DateTime());
            $product->setDateModification(new DateTime());
            $product->setPrix($i . '000€');
            $product->setMargeCommerciale($i . '00€');
            $product->setMargeEntreprise($i . '00€');
            $product->setPrixHT($i . '000€');
            $product->setPrixTTC($i . '000€');
            $product->setEtat($OuvertE);
            $manager->persist($product);
        }

        $product = new Module();
        $product->setName("Module" . $i);
        $product->setDateCreation(new DateTime());
        $product->setDateModification(new DateTime());
        $product->setGamme($Professionels);
        $manager->persist($product);

        $product = new Projet();
        $product->setNom("Projet" . $i);
        $product->setDateCreation(new DateTime());
        $product->setDateModification(new DateTime());
        $product->setClient($Client);
        $product->setCommercial($Commercial);
        $manager->persist($product);



        $manager->flush();
    }
}
